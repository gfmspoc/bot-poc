const csv = require("csv-parser");
const fs = require("fs");

var loadAmounts;

/* Read a CSV file with columns
 *
 *    id, name
 */
loadAmounts = function(filename) {

    return new Promise(function(resolve, reject) {

        var amountsList, amountsMap;

        amountsList = [];

        amountsList.get = function(id) {
            return amountsMap[id];
        };

        amountsMap = {};

        fs.createReadStream(filename)
            .pipe(csv())
            .on("headers", function(headerList) {
                // no op
            })
            .on("data", function(data) {
                amountsList.push(data);
                amountsMap[data.id] = data;
            })
            .on("end", function() {
                resolve(amountsList);
            });
    });

};

module.exports = loadAmounts;