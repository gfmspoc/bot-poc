const csv = require("csv-parser");
const csvWriter = require('csv-write-stream');
const fs = require("fs");

var loadNominations;

/* Read a CSV file with columns
 *
 *    id, nominator_id, nominee_id, approver_id, reason_id, status
 */
loadNominations = function(filename) {

    return new Promise(function(resolve, reject) {

        var nominationsList, nominationMap;

        nominationsList = [];

        nominationsList.get = function(id) {
            return nominationMap[id];
        };

        nominationsList.add = function(nomination) {
            nominationMap[nomination.id] = nomination;
            nominationsList.push(nomination);
        };

        nominationsList.newId = function() {
            var i, max_id;
            max_id = 0;
            for (i = 0; i < nominationsList.length; i++) {
                if (Number(nominationsList[i].id) > max_id) {
                    max_id = Number(nominationsList[i].id);
                }
            }
            return String(max_id + 1);
        };

        nominationsList.save = function() {
            var writer, i;
            writer = csvWriter({
                headers: [ "id", "nominator_id", "nominee_id", "approver_id", "reason_id", "amount_id", "status", "awardMessage" ]
            });
            writer.pipe(fs.createWriteStream(filename));
            for (i = 0; i < nominationsList.length; i++) {
                writer.write(nominationsList[i]);    
            }
            writer.end();
        };

        nominationMap = {};

        fs.createReadStream(filename)
            .pipe(csv())
            .on("headers", function(headerList) {
                // no op
            })
            .on("data", function(data) {
                nominationsList.push(data);
                nominationMap[data.id] = data;
            })
            .on("end", function() {
                resolve(nominationsList);
            });
    });

};

module.exports = loadNominations;