var debug = require('debug')('web-site:authenticationCheck');

var authenticationCheck;

/*
 * Check that the request has a session with a user. If not, redirect to login.
 */
authenticationCheck = function(req, res, next) {

    // if we don't have an existing session and we're trying to do something
    // other than log in, redirect to log in first
    if (!req.session.user && !req.originalUrl.startsWith('/login')) {
        debug('accessing "%s" but not logged in; redirecting to /login', req.originalUrl);
        res.redirect('/login');
        return;
    }

    next();
};

module.exports = authenticationCheck;