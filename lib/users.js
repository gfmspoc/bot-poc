const csv = require("csv-parser");
const csvWriter = require('csv-write-stream');
const fs = require("fs");

var loadUsers;

/* Read a CSV file with columns
 *
 *    id, name, email_address, is_picture_loaded
 */
loadUsers = function(filename) {

    return new Promise(function(resolve, reject) {

        var usersList, userMap;

        usersList = [];

        usersList.get = function(id) {
            return userMap[id];
        };

        usersList.save = function() {
            var writer, i;
            writer = csvWriter({
                headers: [ "id", "name", "email_address", "is_picture_loaded" ]
            });
            writer.pipe(fs.createWriteStream(filename));
            for (i = 0; i < usersList.length; i++) {
                writer.write(usersList[i]);    
            }
            writer.end();
        };

        userMap = {};

        fs.createReadStream(filename)
            .pipe(csv())
            .on("headers", function(headerList) {
                // no op
            })
            .on("data", function(data) {
                usersList.push(data);
                data.is_picture_loaded = ("TRUE" == data.is_picture_loaded) || ("true" == data.is_picture_loaded) ;
                userMap[data.id] = data;
            })
            .on("end", function() {
                resolve(usersList);
            });
    });

};

module.exports = loadUsers;