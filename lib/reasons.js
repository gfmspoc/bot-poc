const csv = require("csv-parser");
const fs = require("fs");

var loadReasons;

/* Read a CSV file with columns
 *
 *    id, name
 */
loadReasons = function(filename) {

    return new Promise(function(resolve, reject) {

        var reasonsList, reasonMap;

        reasonsList = [];

        reasonsList.get = function(id) {
            return reasonMap[id];
        };

        reasonMap = {};

        fs.createReadStream(filename)
            .pipe(csv())
            .on("headers", function(headerList) {
                // no op
            })
            .on("data", function(data) {
                reasonsList.push(data);
                reasonMap[data.id] = data;
            })
            .on("end", function() {
                resolve(reasonsList);
            });
    });

};

module.exports = loadReasons;