# Preamble

Tiny toy app. Maybe the base to build something on top of.


## Deployment and Running

This runs using node. It does not need a database. Start with

```
npm install
start-web-site.bat
```

and navigate to

```
http://localhost:3000/
```


## Functionality

The app requires you to 'log in'. Since this is a toy app, this really just means picking a user from a selection box. 
No password is needed. The set of available users is read from `users.csv`.

### Home

Just shows the awards in the system. Awards are nominations in the `approved` state.


### Nominate

Lets the user create a new nomination. The logged in user is the nominator. They select the nominee, the reason and 
who they want to approve it. Every nomination has one nominee and one approver. 
No checks are done on who is chosen: the nominator may choose themselves as the nominee and approver. That sort of 
security is not the point of this toy.

The new nomination is added to the list in `nominations.csv`.


### Approvals

Lets the user approver or disapprove nominations awaiting their approval, if any.


### Profile

Shows the user's profile picture if they have loaded one. The user can remove the picture or 'load' a new one. 
Loading a picture is really just flipping a flag to say that the user has a picture; the image file is based on the user's 
`id` and is read from the `public/images` folder.


## Data

The app does not need a database to run. Its data is stored in three files. When the data in a file changes, it is overwritten. 
The 'is_picture_loaded' can be changed for a user, causing the system to overwrite `users.csv`. 
The `nominations.csv` is overwritten when a new nomination is created, or the state changes of an existing one.

Clearly this is a stupid decision for anything other than a low-traffic and very simple proof-of-concept. 
But that's what we have here, and it makes the whole thing very self contained.


### users.csv

Lists the users in the system. Is overwritten when any user add/removes a profile picture, to keep this data up to date.


### reasons.csv

The reasons available on the nominate page.


### nominations.csv

The nominations in the system. Each nomination refers to its nominator, nominee, reason and approver by their id. 
The status is one of 'pending', 'approved', 'disapproved'.

The app adds to this list. If adding to the list outside the app, use integers for nomination ids.


## App Structure

Logging in creates a `user` object on the session. If the user attempts to get to a URL (other than `/login`) 
without an active session, they are redirected to `/login`. This is accomplished by `lib/authenticationCheck.js`. 
We don't try to remember the original target URL when we do that.

In the `lib` folder, you will find the files `users.js`, `nominations.js` and `reasons.js`. 
These load the data from the CSV files. The simplest is the `reasons.csv` because it does not need to write that file. 
The others can write as well are read the files.

Outside the `lib` folder, this follows the structure created by Express' generator. 
The files in the `routes` folder handle the requests. The templates in the `views` folder are EJS templates used to 
create HTML for the responses. 

Most pages (not login) have a shared header and (invisible) footer. The footer could contain a Google analytics scriptlet, 
or something like that. Something you want to inject into pages for logged in users.

When the user approves (or disapproves) a nomination, we redirect them back to their `/approvals` page. 
But we also add the hash `done`. So the URI when a nomination is approved/disapproved is different (a bit) from just going 
to the page to see what needs to be done.

## Bot Framework - local debugging

### Step 1 - Pre-requisites

* Ngrok
* .NET

_TODO_

### Step 2 - Setup the Bot Connection

1. Install [`ngrok`](https://ngrok.com/). 
   By default, the Bot Framework SDK will run on port `3984` 
   so you should establish a tunnel by running: `ngrok http 3984 -host-header=localhost:3984`. 
   This should give you an https endpoint. 
1. Go to [Azure Portal](https://portal.azure.com), and create a Bot Channels Registration. 
   Set the `Messaging endpoint` to the endpoint given by `ngrok`, followed by `/api/messages`. 
1. Obtain the Microsoft App Id and Password associated with the bot. 
   You will need this to setup the bot locally. 
   This is located in the `Settings` pane of the Bot Channels Registration. 

> You need to click the external link, and sign-in with your account, to generate a new password, 
if you selected `Auto create App Id and Password` during setup. 


1. Open the `web.config` of the Bot solution and find the following section:

```XML
  <appSettings>
    <!-- update these with your Microsoft App Id and your Microsoft App Password-->
    <add key="MicrosoftAppId" value="" />
    <add key="MicrosoftAppPassword" value="" />
  </appSettings>
```

1. In the Bot Channels Registration, you also need to create a new _channel_, specifically a `Direct Line Channel`. You should note the secret, which will be used in the _app_ project. 
1. You also need to note the Bot ID to whatever you've selected when setting up the registration.
1. Create a file, called `development.js` inside the `config` folder (create it, if it doesn't exist). The file should look like this:

```JavaScript
module.exports = {
  bot : { 
    id: 'your bot id',
    name: 'your bot name',
    directLine: {
      secret: 'your bot secret'
    }
  }
};

```
1. Run `start-web-site.bat` to start the web app, and the Bot solution, to start the bot. 

> The two-way communication is using the Backchannel, which is explained more [here](https://github.com/Microsoft/BotFramework-WebChat#the-backchannel).
