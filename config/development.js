console.log("Direct line secret present: " + (process.env.DIRECT_LINE_SECRET != undefined));

module.exports = {
  bot: {
    id: 'sparkybot',
    name: 'Sparky',
    directLine: {
      secret: process.env.DIRECT_LINE_SECRET
    }
  }
};
