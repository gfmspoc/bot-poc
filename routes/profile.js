var express = require('express');
var router = express.Router();
var debug = require('debug')('web-site:profile');

/***************************************************************************
    Profile page

    If the user has a picture, let them remove it. If they do not have
    a picture, let them upload one.    

***************************************************************************/

router.get('/profile', function(req, res, next) {

    res.render('profile', {
        user: req.session.user
    });
});

router.post('/profile', function(req, res, next) {

    req.session.user.is_picture_loaded = (req.body.action == 'add_profile_picture');

    debug("req.session.user.is_picture_loaded = %o", req.session.user.is_picture_loaded);

    // the user object in the session can be a *copy* of the one in the users list,
    // so we have to copy the updated value of 'is_picture_loaded' to the object
    // on the users list
    req.app.users.get(req.session.user.id).is_picture_loaded = req.session.user.is_picture_loaded;
    req.app.users.save();

    res.redirect('/profile');
});


module.exports = router;
