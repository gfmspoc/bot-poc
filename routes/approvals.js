var express = require('express');
var router = express.Router();
var debug = require('debug')('web-site:approvals');


/***************************************************************************
    Approval page
    
    Present user with a list of nominations awaiting their approval, if any.
    They can POST an 'approve' or 'disapprove' of each one.

***************************************************************************/

router.get('/approvals', function(req, res, next) {

    var app, approvals, i, nomination;

    app = req.app;
    approvals = [];
    if (app.nominations.length > 0) {
        for (i = app.nominations.length-1; i >= 0; i--) {
            if (app.nominations[i].status == 'pending' && app.nominations[i].approver_id == req.session.user.id) {
                nomination = app.nominations[i];
                approvals.push({
                    id: nomination.id,
                    nominator: app.users.get(nomination.nominator_id),
                    nominee: app.users.get(nomination.nominee_id),
                    reason: app.reasons.get(nomination.reason_id),
                    status: 'pending'
                });
            }
        }
    }

    res.render('approvals', {
        user: req.session.user,
        approvals: approvals
    });
});

router.post('/approvals', function(req, res, next) {

    req.app.nominations.get(req.body.nomination_id).status = req.body.newStatus;
    req.app.nominations.save();

    res.redirect('/approvals#done');
});


module.exports = router;
