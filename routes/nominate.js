var express = require('express');
var router = express.Router();
var debug = require('debug')('web-site:nominate');
var config = require("../config/" + (process.env.NODE_ENV || "development") + ".js");

console.log(config.bot.name);

/***************************************************************************
    Nominate page
    
    Present user with ability to create a nomination. Logged in user is the
    nominator. Drop down list for nominee, reason and approver. They POST
    the choices to the server and we create an nomination.

***************************************************************************/

router.get('/nominate', function(req, res, next) {

    res.render('nominate', {
        user: req.session.user,
        users: req.app.users,
        reasons: req.app.reasons,
        config: config,
        amounts: req.app.amounts
    });
});

router.post('/nominate', function(req, res, next) {

    var app, nomination;

    app = req.app;

    // create a new nomination from the info on the request
    var nomination = {
        id: req.app.nominations.newId(),
        nominator_id: req.session.user.id,
        nominee_id: req.body.nominee_id,
        reason_id: req.body.reason_id,
        amount_id: req.body.amount_id,
        approver_id: req.body.approver_id,
        status: 'pending',
		awardMessage: req.body.awardMessage
    };

    // add it to the existing list and save the list
    app.nominations.add(nomination);
    app.nominations.save();
    
    res.render('nominate', {
        user: req.session.user,
        users: req.app.users,
        reasons: req.app.reasons,
        amounts: req.app.amounts,
        config: config
    });
});


module.exports = router;
