var express = require('express');
var router = express.Router();
var debug = require('debug')('web-site:login');
var crypto = require('crypto');


/***************************************************************************
    Login page
    
    Present user with a list of available users. They select one from a drop
    down and POST the selection to the server. That starts a session as that
    user.

***************************************************************************/

router.get('/login', function(req, res, next) {

    req.session.destroy();

    res.render('login', {
        users: req.app.users
    });
});

router.post('/login', function(req, res, next) {

    req.session.user = req.app.users.get(req.body.user_id);
    
    debug("logged in %o", req.session.user);

    res.redirect('/nominate');
});


module.exports = router;
