var express = require('express');
var router = express.Router();
var debug = require('debug')('web-site:index');


/***************************************************************************
    Home page
    
    Shows a list of the awards in the system.

***************************************************************************/

router.get('/', function(req, res, next) {

    var app, awards, i, nomination;

    app = req.app;
    awards = [];
    if (app.nominations.length > 0) {
        for (i = app.nominations.length-1; i >= 0; i--) {
            if (app.nominations[i].status == 'approved') {
                nomination = app.nominations[i];
                awards.push({
                    id: nomination.id,
                    nominator: app.users.get(nomination.nominator_id),
                    nominee: app.users.get(nomination.nominee_id),
                    reason: app.reasons.get(nomination.reason_id),
                    status: 'approved'
                });
            }
        }
    }

    res.render('index', {
        user: req.session.user,
        awards: awards
    });
});


module.exports = router;
